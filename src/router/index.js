import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: () => import('../views/admin/content/Dashboard.vue')
  },
  {
    path: '/pemasukan',
    name: 'Revenue',
     component: () => import( '../views/admin/content/Revenue.vue')
  },
  {
    path: '/pengeluaran',
    name: 'Expenses',

    component: () => import( '../views/admin/content/Expenses.vue')
  },
  {
    path: '/pengingat',
    name: 'Reminders',

    component: () => import( '../views/admin/content/Reminders.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
