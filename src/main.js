import Vue from 'vue'
import App from './App.vue'
import style from '../style/admin.css'
import mdi from '../style/mdi-icons/css/materialdesignicons.css'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import router from './router'

Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(style)
Vue.use(mdi)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
